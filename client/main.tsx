import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { Tracker } from 'meteor/tracker';

// J'importe React et je lui dis de s'afficher dans l'id react-target 
import App from '../imports/ui/components/App';

// La méthode startup se lance quand le DOM est prêt 
Meteor.startup(() => {
  Tracker.autorun(() => {
    // Une fois que j'ai récupérer tout les users 
    const userReady:boolean = Meteor.subscribe('users.all').ready();
    if (userReady) {
      // J'initialise react 
      render(<App />, document.getElementById('react-target'));
    } else{
      console.log("User not ready");
    }
  });
});


