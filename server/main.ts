import { Meteor } from 'meteor/meteor';

import { createDummyUsers, createDummyChats, createDummyMessages } from '../imports/api/helpers';
import { dummyUsers } from '../imports/api/users';
import { dummyChats, ChatsCollection } from '../imports/api/chats';
import { dummyMessages, MessagesCollection } from '../imports/api/messages';


// Startup est appelé quand le DOM est prêt 
Meteor.startup(() => {
    // Je récupère le nombre d'utilisateur connecté
    const numberOfUsers: number = Meteor.users.find().count();

    // Je récupère le nombre de chats 
    const numberOfChats: number = ChatsCollection.find().count();

    // Je récupère le nombre de message 
    const numberOfMessages: number = MessagesCollection.find().count();

    if(numberOfUsers === 0) {
        console.log("Il n'y a pas d'utilisateur");
        // Je créer des faux users 
        createDummyUsers(dummyUsers);
    } else {
        console.log("Il y a ", numberOfUsers, "utilisateurs");
    }

    if(numberOfChats === 0) {
        console.log("Il n'y a pas de chat");
        // Je créer des faux chats 
        createDummyChats(dummyChats);
    } else {
        console.log("Il y a ", numberOfChats, "chats");
    }

    if(numberOfMessages === 0) {
        console.log("Il n'y a pas de Messages");
        // Je créer des faux chats 
        createDummyMessages(dummyMessages);
    } else {
        console.log("Il y a ", numberOfMessages, "Messages");
    }
})