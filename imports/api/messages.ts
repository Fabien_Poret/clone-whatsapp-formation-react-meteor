import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';


import { Message, MessageType } from './models';


import moment from 'moment';

// Je créer une nouvelle collection relative au chats 
export const MessagesCollection = new Mongo.Collection<Message>('Messages');

// Je créer un faux jeu de chats 
export const dummyMessages:Message[] = [
    {
      chatId: "rr6yubZaiy4HwdXuC",
      content: "Salut a va ?",
      createdAt: moment().subtract(2, 'days').toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "CedQ6ok7LQYuLwu7z",
      content: "Salut a va ?",
      createdAt: moment().subtract(2, 'days').toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "rr6yubZaiy4HwdXuC",
      content: "Salut a va ?",
      createdAt: moment().subtract(2, 'days').toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "CedQ6ok7LQYuLwu7z",
      content: "Salut a va ?",
      createdAt: moment().subtract(2, 'days').toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "rr6yubZaiy4HwdXuC",
      content: "Salut a va ?",
      createdAt: moment().subtract(1, 'days').toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "CedQ6ok7LQYuLwu7z",
      content: "Salut a va ?",
      createdAt: moment().subtract(1, 'days').toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "rr6yubZaiy4HwdXuC",
      content: "Salut a va ?",
      createdAt: moment().subtract(1, 'days').toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "rr6yubZaiy4HwdXuC",
      content: "Salut a va ?",
      createdAt: moment().subtract(1, 'days').toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "CedQ6ok7LQYuLwu7z",
      content: "Salut a va ?",
      createdAt: moment().toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "rr6yubZaiy4HwdXuC",
      content: "Salut a va ?",
      createdAt: moment().toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "rr6yubZaiy4HwdXuC",
      content: "Salut a va ?",
      createdAt: moment().toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "CedQ6ok7LQYuLwu7z",
      content: "Salut a va ?",
      createdAt: moment().toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "rr6yubZaiy4HwdXuC",
      content: "Salut a va ?",
      createdAt: moment().toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "CedQ6ok7LQYuLwu7z",
      content: "Salut a va ?",
      createdAt: moment().toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "rr6yubZaiy4HwdXuC",
      content: "Salut a va ?",
      createdAt: moment().toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "CedQ6ok7LQYuLwu7z",
      content: "Salut a va ?",
      createdAt: moment().toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "rr6yubZaiy4HwdXuC",
      content: "Salut a va ?",
      createdAt: moment().toDate(),
      type: MessageType.TEXT,
    },
    {
      chatId: "CedQ6ok7LQYuLwu7z",
      content: "Salut a va ?",
      createdAt: moment().toDate(),
      type: MessageType.TEXT,
    },
  ];

// Je vérifie que le serveur est bien ici 
if(Meteor.isServer){
    // Je publie tout mes chats pour pouvoir les récupérer dans mon main.tsx 
    Meteor.publish('messages.all', () => {
        return MessagesCollection.find();
    });

    Meteor.methods({  
      "message.insert" : function(message) {
        return MessagesCollection.insert(message);
      }
    })


}