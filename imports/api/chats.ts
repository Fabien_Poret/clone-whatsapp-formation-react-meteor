import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

// J'importe l'interface chat du models 
import { Chat } from './models';

import moment from 'moment';
import { mainModule } from 'process';

// Je créer une nouvelle collection relative au chats 
export const ChatsCollection = new Mongo.Collection<Chat>('Chats');

// Je créer un faux jeu de chats 
export const dummyChats:Chat[] = [
    {
        title: "",
        picture: "",
        participants: ["9PMQYCLwnsSj5qiQe", "bt2JfP8MXG3KdJDNz"],
        lastMessage: {
           content: "Salut mon pote",
           createdAt:  moment().toDate(),
        }
    },
    {
        title: "",
        picture: "",
        participants: ["2qktHy38ktrWszXEu", "9PMQYCLwnsSj5qiQe"],
        lastMessage: {
           content: "Salut mon gars",
           createdAt:  moment().subtract(1, 'days').toDate(),
        }
    },
    {
        title: "",
        picture: "",
        participants: ["GzKPwXNCgKRwfexwD", "fktb7SzkGaAx8LyEt"],
        lastMessage: {
           content: "Salut mon bb",
           createdAt:  moment().subtract(2, 'days').toDate(),
        }
    }
];

// Je vérifie que le serveur est bien ici 
if(Meteor.isServer){
    // Je publie tout mes chats pour pouvoir les récupérer dans mon main.tsx 
    Meteor.publish('chats.all', () => {
        return ChatsCollection.find();
    });

    // Une fonction flêché ne permet pas de bénéficier d'un this 
    Meteor.publish('chats.mine', function() {
        return ChatsCollection.find({
            participants: {
                $in: [this.userId]
            }
        });
    });
}

// export default ChatsCollection;