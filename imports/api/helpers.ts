import { User, Chat, Message } from "./models";
import { Accounts } from 'meteor/accounts-base';
import { ChatsCollection } from './chats';
import { Meteor } from 'meteor/meteor';
import { MessagesCollection } from "./messages";

export const createDummyUsers = (users: User[]):void => {
    users.forEach(user => {
        const { username, profile, password } = user;
        Accounts.createUser({
            username,
            password,
            profile
        });
    });
}

export const createDummyChats = (chats: Chat[]):void => {
    chats.forEach(chat => {
        ChatsCollection.insert(chat);
    })
}

export const createDummyMessages = (messages: Message[]):void => {
    messages.forEach(message => {
        MessagesCollection.insert(message);
    })
}

// Je lui passe un tableau de chaine de caractère et la fonction doit me retourner une chaine de caractère 
// Cette fonction me permet de savoir quel autre participant est dans la conversation 
const findOtherId = (participants: string[]):string => {
    // Je récupère mon user id 
    const myId:string = Meteor.userId();
    let otherUserId: string;

    // Si mon ID est égale au tableau 0 de participants alors otherUserId est égale à l'id 1 
    if (myId === participants[0]) {
        otherUserId = participants[1];
    } else {
        // Sinon otherUserId est égale à l'id 0 
        otherUserId = participants[0];
    }
    return otherUserId;
}

// Je trouve un utilisateur depuis son id 
const findOtherUser = (_id:string):User => {
    return Meteor.users.findOne({
        _id
    })
}

// Je récupère un chat 
export const findChats = ():Chat[] => {
    // Je récupère tout les chats à laquel j'ai souscris 
    return ChatsCollection.find().fetch()
        // Pour chaque chat je 
        .map(chatsCollection => {
            // Récupère l'id de l'autre participant 
            const otherUserId:string = findOtherId(chatsCollection.participants);
            // Je récupère les informations de l'autre participant et je destrucure pour récupérer uniquement l'username et le profile 
            const { username, profile } = findOtherUser(otherUserId);
            // et je retourne le chat, l'username et le profil de mon autre participant 

            const lastMessage:Message = findLastMessage(chatsCollection._id);
            return {
                ...chatsCollection,
                title: username,
                picture: profile.picture,
                lastMessage: {
                    ...lastMessage
                }
            }
     })
}

const findLastMessage = (chatId:string):Message => {
    return MessagesCollection.find({chatId}, {
        sort: {ceatedAt: -1}
    }).fetch()[0];
}