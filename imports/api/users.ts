import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

import { User } from "./models";

export const dummyUsers:User[] = [
    {
        _id: '0',
        username: 'Ethan Gonzalez',
        password: "password",
        profile: {
            phone: '+222222222',
            picture: 'https://randomuser.me/api/portraits/thumb/men/1.jpg',
            actu: "Salut j'utilise whatsapp"
        }
    }, {
        _id: '1',
        username: 'Bryan Wallace',
        password: "password",
        profile: {
            phone: '+333333333',
            picture: 'https://randomuser.me/api/portraits/thumb/lego/1.jpg',
            actu: "Salut j'utilise whatsapp"
        }
    }, {
        _id: '2',
        username: 'Avery Stewart',
        password: "password",
        profile: {
            phone: '+444444444',
            picture: 'https://randomuser.me/api/portraits/thumb/women/1.jpg',
            actu: "Salut j'utilise whatsapp"
        }
    }, {
        _id: '3',
        username: 'Katie Peterson',
        password: "password",
        profile: {
            phone: '+555555555',
            picture: 'https://randomuser.me/api/portraits/thumb/women/2.jpg',
            actu: "Salut j'utilise whatsapp"
        }
    }, {
        _id: '4',
        username: 'Ray Edwards',
        password: "password",
        profile: {
            phone: '+666666666',
            picture: 'https://randomuser.me/api/portraits/thumb/men/2.jpg',
            actu: "Salut j'utilise whatsapp"
        }
    }, {
        _id: '5',
        username: 'Samy Smith',
        password: "password",
        profile: {
            phone: '+777777777',
            picture: 'https://randomuser.me/api/portraits/thumb/men/1.jpg',
            actu: "Salut j'utilise whatsapp"
        }
    }, {
        _id: '6',
        username: 'John Smith',
        password: "password",
        profile: {
            phone: '+888888888',
            picture: 'https://randomuser.me/api/portraits/thumb/men/2.jpg',
            actu: "Salut j'utilise whatsapp"
        }
    }, {
        _id: '7',
        username: 'Adrianna Scott',
        password: "password",
        profile: {
            phone: '+999999999',
            picture: 'https://randomuser.me/api/portraits/thumb/women/1.jpg',
            actu: "Salut j'utilise whatsapp"
        }
    }, {
        _id: '8',
        username: 'Julienne Smith',
        password: "password",
        profile: {
            phone: '+999999999',
            picture: 'https://randomuser.me/api/portraits/thumb/women/2.jpg',
            actu: "Salut j'utilise whatsapp"
        }
    }, {
        _id: '9',
        username: 'Marco',
        password: "password",
        profile: {
            phone: '+101010101',
            picture: 'https://randomuser.me/api/portraits/thumb/men/1.jpg',
            actu: "Salut j'utilise whatsapp"
        }
    }
]

// Je m'assure que nous sommes biens sur le server 
if (Meteor.isServer) {
    // Je publie tout les users 
    Meteor.publish('users.all', () => {
        return Meteor.users.find({}, {
            // Je m'assure que tout soit publier sauf les mdp 
            fields: {services: 0}
        })
    })
}

Meteor.methods({
    'user.login': ({username, phone, password }) => {
        let userExist: Boolean;

        // findUserByUsername est une méthode côté serveur 
        const user: User = Accounts.findUserByUsername(username);
        // Le double !! permet d'éviter que userExist soit null 
        // L'inverse de null c'est true
        userExist = !!user;
        if (userExist) {
            console.log('User exist', user);
            return true;
        } else {
            console.log('User dont exist');
            
            // Si l'utilisateur n'existe pas alors le créer 
            return Accounts.createUser({
                username,
                password,
                profile: {
                    phone,
                    actu: "J'utilise whatsapp",
                    picture :"https://randomuser.me/api/portraits/thumb/men/1.jpg"
                }
            })
        }
    }
})