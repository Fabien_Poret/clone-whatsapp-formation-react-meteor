import styled from 'styled-components';

const StyledMain = styled.div`
    display: flex;
    flex-direction: row;
    width: 100VW;
    height: 100VH;
`;

export default StyledMain;