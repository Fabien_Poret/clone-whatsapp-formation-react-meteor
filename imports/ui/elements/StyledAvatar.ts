import styled from 'styled-components';

const StyledAvatar = styled.div`
    display: flex;
    width: 4rem;
    height: 4rem;
    border-radius: 2rem;
    position: relative;
    overflow: hidden;
    background: ${({ theme }) => theme.avatar.color.background};
    justify-content: center;
    align-items: center;
    cursor: pointer;

    ${props => props.large && css `
        width: 4.9rem;
        height: 4.9rem;
        border-radius: 2.45rem;
    `}

    .avatar--img {
        width: 100%;
        heigth: 100%;
    }
`;

export default StyledAvatar;