import React from 'react'

import FormLogin from './FormLogin';
import RightImg from './RightImg';

import { Meteor } from 'meteor/meteor';
// import { Accounts } from 'meteor/accounts-base';

const messageText:string = "Connectez vous afin de lancer une conversation";

const Login = (props:any):JSX.Element => {

    // Je récupère mes states dans handlelogin 
    const handleLogin = (state:any):void => {
        console.log(state);
        
        // Je destructure mes states 
        const {username, phone, password } = state;

        // J'appel la fonction user.login de mon user
        Meteor.call('user.login', state, (err, res) => {
            if (err) {
                console.log('Error login', err);
            } else{
                Meteor.loginWithPassword(username, password, (err) => {
                    if (err) {
                        console.log(err);   
                    } else {
                        console.log(res);

                        // Je redirige l'utilisateur qui est connecté dans la route chats
                        props.history.push('/chats');
                    }
                });
            }
        })
    }

    return (
        <RightImg messageText={messageText}>
            {/* Quand je click sur le bouton connexion de mon composant formLogin alors j'appel la fonction handleLogin de ce composant  */}
            <FormLogin onLogin={handleLogin}/>
        </RightImg>
    )
}

export default Login;