import React from 'react'
import Left from './Left';
import Right from './Right';
import StyledMain from '../elements/StyledMain';
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import { withTracker } from 'meteor/react-meteor-data';

import { ChatsCollection } from '../../api/chats';
import { findChats } from '../../api/helpers';
import { Chat } from '../../api/models';

import _ from 'lodash';

const Main = (props:any):JSX.Element => {

    // Permet d'éviter que la page se charge sans les chats et messages 
    // const [loading, setLoading] = React.useState<boolean>(true);
    // Tracker.autorun(() => {
    //     const chatsReady:boolean =  Meteor.subscribe('chats.mine').ready();
    //     const messagesReady:boolean = Meteor.subscribe('messages.all').ready();

    //     if(chatsReady && messagesReady)
    //     {
    //         setLoading(false);
    //     }
    // });

    const [messageVisible, setMessageVisible] = React.useState<boolean>(false);
    const [selectedChat, setSelectedChat] = React.useState<Chat>({});

    const handleChatClick = (_id:string):void => {
        console.log(_id);
        if (!messageVisible) {
            setMessageVisible(true);
        }
        
        // la méthode lodash find permet de trouver a partir d'un id son tableau
        const newChat:Chat = _.find(props.chats, {_id});
        console.log('Selected chat after', newChat);
        setSelectedChat(newChat);
    }

    return (
        <>
            {
                !props.loading ? (
                    <>
                        <StyledMain>
                            <Left chats={props.chats} onChatClick={handleChatClick} selectedChat={selectedChat} />
                            <Right right messageVisible={messageVisible} selectedChat={selectedChat} />
                        </StyledMain>
                    </>
                ): ''
            }

        </>
    )
}

export default withTracker( () => {
    const chatsReady:boolean =  Meteor.subscribe('chats.mine').ready();
    const messagesReady:boolean = Meteor.subscribe('messages.all').ready();

    return{ 
        loading : chatsReady && messagesReady ? false : true,
        chats: findChats()
    }
})(Main);