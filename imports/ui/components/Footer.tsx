import React, { ChangeEvent } from 'react'
import StyledFooter from '../elements/StyledFooter';
import FontAwesome from 'react-fontawesome';

const Footer = (props:any):JSX.Element => {

    const [inputValue, setInputValue] = React.useState<string>('');
    const [iconName, setIconName] = React.useState<string>('microphone');

    const handleChange = (event:ChangeEvent<HTMLInputElement>):void => {
        event.preventDefault();
        setInputValue(event.target.value);

        // condition terner
        const name:string = event.target.value !== "" ? "paper-plane" : "microphone";
        setIconName(name);
    }

    const handleclick = ():void => {
        if (iconName === "microphone") {
            return;
        } 
        // J'appel une fonction de mon composant parent 
        props.onSend(inputValue);
        setInputValue('');
    }

    return (
        <StyledFooter>
            <FontAwesome 
                className="iconFooter"
                name="smile"
            />
            <label htmlFor="" className="message--label">
                <input 
                    className="message--input"
                    value={inputValue}
                    onChange={handleChange}
                    type="text"
                    placeholder="Taper un message"    
                />
            </label>
            <FontAwesome
                className="iconFooter"
                name={iconName}
                onClick={handleclick}
            />
        </StyledFooter>
    );
}

export default Footer;