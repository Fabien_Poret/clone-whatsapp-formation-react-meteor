import React from 'react'

// J'importe le style du composant right img
import StyledRightImg from '../elements/StyledRightImg';

const RightImg = (props:any):JSX.Element => {
    return(
        // Je créer une balise pour que chaque élément intégré dedans possède les propriétés CSS dont j'ai besoin 
        // Je lui passe une props.right 
        <StyledRightImg right={props.right}>
            <img 
                src="./images/whatsapp-bg-1.jpg" 
                alt="bg"
                className="rightImg--image"
            />
            <h3 className="rightImg--title">Gardez votre téléphone connecté</h3>
            <div className="rightImg--div">
                <p className="rightImg--p">
                    {/* Je récupère la props messageText et je l'affiche  */}
                    {props.messageText}
                </p>
                <div className="rightImg--divider" />
            </div>

            {/* Permet d'afficher les enfants de RightImg depuis Login  */}
            {props.children}
        </StyledRightImg>
    )
}

export default RightImg;