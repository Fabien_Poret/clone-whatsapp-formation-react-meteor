import React from 'react';
import StyledLeft from '../elements/StyledLeft';
import Header from './Header';
import Avatar from './Avatar';

import { Meteor } from 'meteor/meteor';
import Status from './Status';
import SearchBar from './SearchBar';
import ChatList from './ChatList';

// icons est un tableau de chaine de caractère 
const icons:string[] = ["circle-notch", "comment-alt", "ellipsis-v"];

const Left = (props:any):JSX.Element => {

    const { chats, onChatClick, selectedChat } = props;

    return (
        <StyledLeft>
            {/* Je passe mes icons et ma classe en props à header  */}
            <Header icons={icons} iconClass="greyIcon">
                
                {/* Je passe en props le picture de l'utilisateur connecté  */}
                <Avatar avatar_url={Meteor.user().profile.picture} />
            </Header>
            <Status />
            <SearchBar />
            <ChatList 
                chats={props.chats} 
                onChatClick={onChatClick} 
                selectedChat={selectedChat}
            />
        </StyledLeft>
    );
}

export default Left;