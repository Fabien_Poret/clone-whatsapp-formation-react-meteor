import React from 'react'
import StyledChatItem from '../elements/StyledChatItem'

import Avatar from './Avatar';
import Moment from 'react-moment';
import moment from 'moment';

const ChatItem = (props:any):JSX.Element => {

    console.log(props);
    // Je destructure mes props 
    const { title, picture, lastMessage, _id, onChatClick, active } = props;
    const { content, createdAt } = lastMessage;

    // Je créer une date à la date d'aujourd'hui avec ce format 
    const now:string = moment().format("D/MM/y");

    // Si now est égale à la date de createdAt avec le même format alors today = à true sinon false
    const today:boolean = now === moment(createdAt).format("D/MM/y");

    // const onChatClick = (_id) => {
    //     return ()
    // }

    return (
        <StyledChatItem active={active} onClick={() => onChatClick(_id)}>
            <Avatar avatar_url={picture}/>
            <div className="chat--contentContainer">
                <div className="content-line1">
                    <span className="content--line1__title">
                        { title } - { _id }
                    </span>
                    <div className="content--line1__date">

                        {/* Si la date est égale à aujourd'hui alors  */}
                        {today ? (
                            <Moment format="HH:mm">
                                { createdAt }
                            </Moment>
                        ) : (
                            <Moment format="D/MM/y">
                                { createdAt }
                            </Moment>
                        )}
                    </div>
                </div>
                <div className="content-line1">
                    <span className="content--message">
                        { content }
                    </span> 
                    <div className="chat--badge">
                        5
                    </div>
                </div>
            </div>
        </StyledChatItem>
    )
}

export default ChatItem;