import React from 'react'

// J'importe le style du composant form login 
import StyledFormLogin from '../elements/StyledFormLogin';

const FormLogin = (props:any):JSX.Element => {

    // Je créer mes states pour mon form de connexion 
    // Je type mon form de type any pour éviter les bugs 
    // Un bug m'oblige à utiliser React.useState plutôt que useState directement 
    const [state, setState] = React.useState<any>({
        username: '',
        phone: '',
        password: ''
    });
    // J'indique que mes propriétés font parties du state, cela me permet d'avoir un raccourcis pour y avoir accès 
    const {username, phone, password} = state;

    // Je récupère en paramètre l'événement de mon formulaire qui sera de type React à chaque changement d'event et ce sera un input de type HTML 
    // Cet élément ne retourne rien donc je le type de void 
    const handleChange = (event:React.ChangeEvent<HTMLInputElement>):void => {
        // Je récupère la valeur de mes champs 
        const inputValue:string = event.target.value;

        // Je récupère le nom de mes champs 
        const inputName:string = event.target.name;

        // J'assigne à mes précédentes states
        setState(prevState => ({
            // Je copie mes précédentes states 
            ...prevState,
            // J'assigne les changements 
            [inputName]: inputValue
        }))
    };

    return (
        // Je créer une balise pour que chaque élément intégré dedans possède les propriétés CSS dont j'ai besoin 
        <StyledFormLogin>
            <label className="label">
                <input 
                    className="input" 
                    name="username" 
                    placeholder="Nom d'utilisateur"
                    value={username}
                    onChange={handleChange}
                />
            </label>
            <label className="label">
                <input 
                    className="input" 
                    name="phone" 
                    placeholder="Téléphone"
                    value={phone}
                    onChange={handleChange}
                />
            </label>
            <label className="label">
                <input 
                    className="input" 
                    name="password" 
                    placeholder="Mot de passe"
                    type="password"
                    value={password}
                    onChange={handleChange}
                />
            </label>
            {/* Quand je click sur le bouton, j'appel la fonction de la props onLogin du composant parent Login  */}
            {/* Je passe à ma props tout mes states  */}
            <button className="loginBtn" onClick={() => props.onLogin(state)}>CONNEXION</button>
        </StyledFormLogin>
    )
}

export default FormLogin;