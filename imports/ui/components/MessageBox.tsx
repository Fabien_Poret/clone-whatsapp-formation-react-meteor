import React from 'react'
import StyledMessageBox from '../elements/StyledMessageBox'
import { Meteor } from 'meteor/meteor';

import moment from 'moment';
import _ from 'lodash';
import Day from './Day';
import { render } from 'react-dom';
import MessageText from './MessageText';

import FlipMove from 'react-flip-move';

let isEven:boolean = false;
const format:string = 'DD/MM/y';

let messagesEnd:HTMLDivElement;

const MessageBox = (props:any):JSX.Element => {

    const { messages, selectedChat } = props;

    messages.forEach(message => {
        if (!message.senderId) {
            // Savoir si c'est mon message ou celui d'un autre 
            message.ownership = !!message.ownership === isEven ? 'mine' : 'other';
            isEven = !isEven;
            return message;
        } else {
            message.ownership = message.senderId === Meteor.userId() ? 'mine' : 'other';
            return message;
        }
    });


    // Permet de trier le tableau par date 
    // Transforme le tableau message en objet 
    const groupedMessages:any = _.groupBy(messages, message => {
        return moment(message.createdAt).format(format);
    })


    // Permet de récupérer les dates (keys) des messages 
    // Permet de retourner un tableau plutôt qu'un objet 
    const newMessages:any[] = Object.keys(groupedMessages).map(key => {
        return { 
            date: key,
            groupedMessages: groupedMessages[key],
            today: moment().format(format) === key
        }
    });

    console.log(newMessages);

    // Je retourne une tableau d'element 
    const renderDays = ():JSX.Element[] => {
        // Je récupère grâce à la fonction map le message et l'index 
        // je return une div et un message pour chaque element de newMessages 
        // Permet de savoir si c'est la date d'aujourd'hui 
        
        return newMessages.map((message, index:number) => {
            const dateText:string = message.today ? "Aujourd'hui" : message.date;
            return (
            <div key={index}>
                <Day date={dateText} />
                {renderMessages(message)}
            </div>
            )
        })
    }

    const renderMessages = (message:any):JSX.Element[] => {
        console.log(message);
        return message.groupedMessages.map(message => {
            const msgClass:string = `message message--${message.ownership}`;
            return (
                <MessageText  
                    key={message._id}
                    msgClass={msgClass}
                    content={message.content}
                    ownership={message.ownership}
                    createdAt={message.createdAt}
                />
            )
        })
    }

    const scrollToBottom = ():void => {
        messagesEnd.scrollIntoView({behavior: "smooth"});
    }

    // Au chargement de la page et quand selectedchat et messages change 
    React.useEffect(() => {
        scrollToBottom();
    }, [selectedChat, messages])

    return (
        <StyledMessageBox>
            <FlipMove>
                {renderDays()}
            </FlipMove>
            {/* Toujours utiliser une fonction pour appeler un composant plusieurs fois */}
            <div ref={(el:HTMLDivElement) => messagesEnd = el} />
        </StyledMessageBox>
    )
}

export default MessageBox;