import React from 'react';
import StyledHeader from '../elements/StyledHeader';
import FontAwesome from 'react-fontawesome';

const Header = (props:any):JSX.Element => {

    // Je destructure mes props 
    const {icons, iconClass} = props;

    // renderIcons retourne un tableau d'element JSX 
    const renderIcons = ():JSX.Element[] => {
        return icons.map((icon:string, i:number) => {
            return (
                <FontAwesome
                    key={i}
                    className={iconClass}
                    name={icon}
                />
            )
        })
    }

    return (
        <StyledHeader>
            {/* Permet d'afficher les enfants de Avatar depuis Left  */}
            {props.children}

            {/* Si la props.iconWidthSmall existe alors passer le premier string sinon le second  */}
            <div className={props.iconWidthSmall ? "icons--left small" : "icons--left"}>
                {/* J'appel ma méthode renderIcons  */}
                {renderIcons()}
            </div>
        </StyledHeader>
    );
}

export default Header;