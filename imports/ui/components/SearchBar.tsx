import React from 'react'
import StyledSearchbar from '../elements/StyledSearchBar'
import FontAwesome from 'react-fontawesome';


const SearchBar = (props:any):JSX.Element => {
    return (
        <StyledSearchbar>
            <label className="searchbar--label">
                <FontAwesome
                    name="search"
                    className="searchbar--icon"
                />
                <input
                    className="searchbar--input"
                    placeholder="Rechercher pour démarrer une discussion"
                />
            </label>
        </StyledSearchbar>
    )
}

export default SearchBar;