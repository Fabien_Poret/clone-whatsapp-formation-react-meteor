import React from 'react'
import StyledMessageView from '../elements/StyledMessageView'
import Header from './Header';
import Avatar from './Avatar';
import { Chat, Message, MessageType } from '../../api/models';
import Footer from './Footer';
import MessageBox from './MessageBox';
import { Tracker } from 'meteor/tracker';
import { MessagesCollection } from '../../api/messages';

import moment from 'moment';
import { Meteor } from 'meteor/meteor';

import { withTracker } from 'meteor/react-meteor-data';


const icons:string[] = ["search", "paperclip", "ellipsis-v"];

const MessageView = (props:any):JSX.Element => {



    console.log(props);

    const selectedChat = props.selectedChat;

    let messages:Message[];

    const handleSend = (content:string):void => {
        const message:Message = {
            chatId: selectedChat._id,
            content,
            createdAt: moment().toDate(),
            senderId: Meteor.userId(),
            type: MessageType.TEXT,
            read: false
        }
        Meteor.call('message.insert', message, (err, res) => {
            if (err) {
                console.log(err);   
            } else {
                console.log('res', res);
            }
        })
    }

    return (
        <StyledMessageView>
            <Header iconClass="greyIcon" icons={icons}>
                <Avatar avatar_url={selectedChat.picture} />
                <div className="headerMsg--container">
                    <span className="headerMsg--title">
                        {selectedChat.title}
                    </span>
                    <span className="headerMsg--sbTitle">
                        en ligne
                    </span>
                </div>
            </Header>
            <MessageBox selectedChat={selectedChat} messages={props.messages} />
            <Footer onSend={handleSend}/>
        </StyledMessageView>
    )
}

export default withTracker(({selectedChat}) => {
    console.log('withtracker');
    return {
        messages: MessagesCollection.find({chatId: selectedChat._id}).fetch()
    }
})(MessageView);
