import React from 'react';
// J'importe les éléments nécessaire pour faire des routes en single page 
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// J'importe le module theme
import { ThemeProvider } from "styled-components";
import theme from '../theme/NormalTheme';

// J'importe mes composants 
import Login from './Login';
import Main from './Main';

const App = (props:any):JSX.Element => {
  return (
    <ThemeProvider theme={theme}>
      <Router>
          <Switch>
              {/* La route / appartient au composant Login  */}
              {/* L'attribut exact permet d'éviter les conflits de route parents enfants  */}
              <Route path="/" component={Login} exact />
              <Route path="/chats" component={Main} exact />
          </Switch>
      </Router>
    </ThemeProvider>
  )
};

export default App;
